#!/usr/bin/env bash

blue(){ echo -e "$(tput bold; tput setaf 6)${*}$(tput sgr0)"; }
msg(){ echo -e "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"; }
err(){ echo "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)"; }

btl='bluetoothctl'
sel1=$(mktemp -uq)
sel2=$(mktemp -uq)
devices=$(mktemp -uq)
ids=$(mktemp -uq)
lines=$(mktemp -uq)
all=$(mktemp -uq)

trap 'rm -f $sel1 $sel2 $devices $ids $lines $all' EXIT

cat <<-EOF >$sel1
   Scan for Devices
   Connect to Device
   Activate Bluetooth
   Deactivate Bluetooth
EOF

# Devices:

echo "$($btl devices|nl|sed 's/\s*//' |sed 's/\s*Device//'|cut -d\  -f1)" >> $lines
echo "$($btl devices|nl|sed 's/\s*//' |sed 's/\s*Device//'|cut -d\  -f2)" >> $ids
echo "$($btl devices|nl|sed 's/\s*//' |sed 's/\s*Device//'|cut -d\  -f3-)" >> $devices
echo "$($btl devices|nl|sed 's/\s*//' |sed 's/\s*Device//')" >> $all

# Main :

connectfunc(){
  $btl connect $1 && exit 0 ||
  $btl trust $1 &&
  $btl pair $1 &&
  $btl connect $1 && exit 0
}

activatefunc(){
  $btl power on  >/dev/null 2>&1 && msg "Changing power on succeeded ✔" || err "Changing power on failed ✖"
  $btl discoverable on >/dev/null 2>&1 && msg "Changing discoverable on succeeded ✔" || err "Changing discoverable on failed ✖"
  $btl pairable on >/dev/null 2>&1 && msg "Changing pairable on succeeded ✔" || err "Changing pairable on failed ✖"
  $btl agent on >/dev/null 2>&1 && msg "Changing agent on succeeded ✔"
  $btl default-agent on >/dev/null 2>&1 && msg "Changing default-agent on succeeded ✔"
}


sel1func(){
  clear ; echo -e "\n" ;
  pair="$(cat $sel1|fzf \
    --prompt="Select an option:                         " \
    --header=" " \
    --height=12 \
    --margin=10%,30% \
    --layout=reverse \
    --border \
    --color=fg:-1,bg:-1,hl:6,fg+:2,bg+:0,hl+:6 \
    --pointer=▶ \
    --marker=✓ \
    --cycle \
    --reverse \
    --no-info \
    --no-hscroll \
    --no-sort \
    --inline-info \
  )"



clear && echo -e "\n"
case $pair in
  *Device) sel2func ;;
  *Activate*) activatefunc ;;
  *Scan*) clear &&  echo -e "";  msg "Scanning for devices...\n" && $btl scan on ;;
  *Deactivate*) $btl power off && msg "Changing power off succeeded ✔" || err "Changing power off failed ✖" ;;
  *) echo "Not found" ;;
esac ; }



# Devices:

echo "$($btl devices|nl|sed 's/\s*//' |sed 's/\s*Device//'|cut -d\  -f1)" >> $lines
echo "$($btl devices|nl|sed 's/\s*//' |sed 's/\s*Device//'|cut -d\  -f2)" >> $ids
echo "$($btl devices|nl|sed 's/\s*//' |sed 's/\s*Device//'|cut -d\  -f3-)" >> $devices

sel2func(){

  clear ; echo -e "\n"

  dev="$(cat $devices|fzf \
    --height=20 \
    --margin=18%,40%,40%,40% \
    --color bg:"#222222",preview-bg:"#333333" \
    --no-extended \
    --layout reverse \
    --header " " \
    --prompt "          Select Device :" \
    --no-info \
    --disabled \
    --border \
    --bold \
    --keep-right)"


  while IFS=\   read -r  line id device; do
		n=$((n+1))
		case "$dev" in
      "$device") ! connectfunc $id && err "Failed to connect to $device ✖" || msg "Connected to $device ✔" ;;
    esac
  done < $all
}

sel1func

